<?php
/**
* The events-load script is used to load all events from the database into the session, to be used by various other scripts.
*
* @author Liam Harbec, 2020, liam.harbec13@gmail.com
*/
?>

<?php
    $result = array();
    
    // Assume $name, $host, $user, and $password have been properly assigned
    try {
        $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
        $query = 'SELECT * from wp2h_cf_form_entry_values';
        $stmt = $pdo->prepare($query);
        $stmt->execute();
        $result = $stmt->fetchAll();
    } catch (PDOException $e) {
        echo 'Please contact the website administrators: \n\n'.$e->getMessage();
    } finally {
        unset($pdo);
    }
    $events = array();

    // Each row in wp2h_cf_form_entry_values
    foreach ($result as $row) {
        $id = $row['entry_id'];
        $slug = $row['slug'];
        $value = $row['value']; 
        // If event with that id is already existing, add the new field to it
        if (array_key_exists($id, $events)) {
            $events[$id] += [$slug => $value];
        }
        // If event with that id is does not yet exist, create it and add the field
        else {
            $events[$id] = array($slug => $value);
            $events[$id] += ['entry_id' => $id];
        }
    }

    $_SESSION['events'] = $events;
?>

<?php
/**
* The management-events script is used to generate the entire management side of the website.
* It creates all of the HTML elements, and allows the modification and approval of events stored in the database.
* It also handles the creation of event posts for each event that gets approved.
*
* @author Liam Harbec, 2020, liam.harbec13@gmail.com
*/
?>

<style type="text/css">
    input[type="submit"], input[type="button"] {
        border: 1px solid #DDDDDD;
        background-color: white;
        color: #6D6D6D;
        border-radius: 3px;
        height: 30px;
        padding: 5px 15px 5px 15px;
        font-weight: normal;
    }
    
    input[type="submit"]:hover, input[type="button"]:hover {
        background-color: #E7E7E7;
        border: 1px solid #AAAAAA;
        color: #3C4655;
    }
</style>

<script type="text/javascript">
    /**
    * Inserts all data from the inputted event into the input fields
    */
    function editEvent(event) {
        document.getElementById("i_event_name").value = event["event_name"];
        document.getElementById("i_event_name").disabled = false;
        document.getElementById("i_event_address").value = event["event_address"];
        document.getElementById("i_event_address").disabled = false;
        document.getElementById("i_start_date").value = event["start_date"];
        document.getElementById("i_start_date").disabled = false;
        if (typeof event["end_date"] === 'undefined') {
            document.getElementById("i_end_date").value = "";
        } else {
            document.getElementById("i_end_date").value = event["end_date"];
        }
        document.getElementById("i_end_date").disabled = false;
        document.getElementById("i_start_time").value = event["start_time"];
        document.getElementById("i_start_time").disabled = false;
        document.getElementById("i_end_time").value = event["end_time"];
        document.getElementById("i_end_time").disabled = false;
        document.getElementById("i_contact_name").value = event["contact_name"];
        document.getElementById("i_contact_name").disabled = false;
        document.getElementById("i_contact_email").value = event["contact_email"];
        document.getElementById("i_contact_email").disabled = false;
        // If the field was not submitted through the form
        if (typeof event["contact_phone"] === 'undefined') {
            document.getElementById("i_contact_phone").value = "";
        } else {
            document.getElementById("i_contact_phone").value = event["contact_phone"];
        }
        document.getElementById("i_contact_phone").disabled = false;
        
        var english_desc = event["event_description_english"];
        var spanish_desc = event["event_description_spanish"];
        var french_desc = event["event_description_french"];
        var notes = event["notes"];
        
        document.getElementById("i_event_description_french").value = french_desc.replace(/<br\s*[\/]?>/gi, "\n");
        document.getElementById("i_event_description_french").removeAttribute('readonly');
        document.getElementById("i_event_description_spanish").value = spanish_desc.replace(/<br\s*[\/]?>/gi, "\n");
        document.getElementById("i_event_description_spanish").removeAttribute('readonly');
        document.getElementById("i_event_description_english").value = english_desc.replace(/<br\s*[\/]?>/gi, "\n");
        document.getElementById("i_event_description_english").removeAttribute('readonly');
        document.getElementById("i_target_audience").value = event["target_audience"];
        document.getElementById("i_target_audience").disabled = false;
        document.getElementById("i_price").value = event["price"];
        document.getElementById("i_price").disabled = false;
        
        // If there was no attached image
        if (event["event_brochure"].length == 0) {
            document.getElementById("i_event_brochure").value = "https://moisheritagelatinoamericain.ca/wp-content/uploads/2020/default/default_brochure.jpg";
        } else {
            document.getElementById("i_event_brochure").value = event["event_brochure"];
        }
        document.getElementById("i_event_brochure").disabled = false;
        document.getElementById("i_event_brochure_img").src = document.getElementById("i_event_brochure").value;
        
        // If the field was not submitted through the form
        if (typeof event["notes"] === 'undefined') {
            document.getElementById("i_notes").value = "";
        } else {
            document.getElementById("i_notes").value = notes.replace(/<br\s*[\/]?>/gi, "\n");
        }
        document.getElementById("i_notes").removeAttribute('readonly');
        document.getElementById("i_entry_id").value = event["entry_id"];
        
        document.getElementById("i_column").value = event['column'];
        
        document.getElementById("i_postid_en").value = event["post_id_en"];
        document.getElementById("i_postid_es").value = event["post_id_es"];
        document.getElementById("i_postid_fr").value = event["post_id_fr"];
        alert('La información ha sido ingresada.');
    }

    /**
    * Prompts the user to confirm they would like to approve/modify the event
    */
    function confirmApprove() {
        if (confirm("¿Está seguro de que desea aprobar o modificar el evento?")) {
            return true;
        } else {
            return false;
        }
    }
    
    /**
    * Prompts the user to confirm they would like to un-approve the event
    */
    function confirmUnapprove() {
        if (confirm("¿Está seguro de que desea desaprobar el evento?")) {
            return true;
        } else {
            return false;
        }
    }

    /**
    * Prompts the user to confirm they would like to delete the event
    */
    function confirmDelete() {
        if (confirm("¿Está seguro de que desea eliminar el evento?")) {
            return true;
        } else {
            return false;
        }
    }

</script>

<?php
    // Will be true if they were logged in successfully
    if ($_SESSION['manager_login'] == true) {
        if (isset($_SESSION['events'])) {
            // Gets all events in the database
            
            $events = $_SESSION['events'];
            
            loadInputs();
            
            // If the Aprobar button was pressed
            // Creates the post, approves in the database
            if (isset($_POST['approve'])) {
                $isCancelled = approveEvent($_POST['i_column']);
                if ($isCancelled) {
                    unset($_POST['approve']);
                }
            }
            
            echo "<div style='display: flex;'>";
            
            echo "<div style='flex-grow: 1'>";
            loadEvents($events, 0, "Nuevos eventos");
            echo "</div>";
            
            echo "<div style='flex-grow: 1'>";
            loadEvents($events, 1, "Eventos aprobados");
            echo "</div>";
            
            echo "</div>";
        }
    }
    // User is not logged in to the management side of the website
    else {
        echo "<h2>No tiene acceso para ver esta parte del sitio web.</h2>";
        echo "<p>Presione el botón de abajo para volver a la página de inicio.</p>";
        echo "<button onclick=\"location.href = 'https://moisheritagelatinoamericain.ca/es/inicio'\">Inicio</button>";
    }

    /**
    * Query to take the data from the input tags above and replace the data in the database
    */
    function updateFieldQuery($field, $entry_id, $value) {
        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = 'UPDATE wp2h_cf_form_entry_values SET value = ? WHERE entry_id = ? AND slug = ?';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $value);
            $stmt->bindValue(2, $entry_id);
            $stmt->bindValue(3, $field);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

    /**
    * Query to change the approved value from 0 to 1, to approve the event
    */
    function approveEventQuery($entry_id) {
        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = 'UPDATE wp2h_cf_form_entry_values SET value = 1 WHERE entry_id = ? AND slug = \'approved\'';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $entry_id);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

    /**
    * Query to change the approved value from 1 to 0, to un-approve the event
    */
    function unapproveEventQuery($entry_id) {
        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = 'UPDATE wp2h_cf_form_entry_values SET value = 0 WHERE entry_id = ? AND slug = \'approved\'';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $entry_id);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

    /**
    * Query to delete an event from the database
    */
    function deleteEventQuery($event) {
        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = 'DELETE FROM wp2h_cf_form_entry_values WHERE entry_id = ?';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $event['entry_id']);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

    /**
    * Query to add the post id
    */
    function addPostIdQuery($postId, $language) {
        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = 'INSERT INTO wp2h_cf_form_entry_values (entry_id, field_id, slug, value) VALUES (?, ?, ?, ?)';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $_POST['i_entry_id']);
            $stmt->bindValue(2, "fld_postid_".$language);
            $stmt->bindValue(3, "post_id_".$language);
            $stmt->bindValue(4, $postId);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

    /**
    * Query to delete the post id reference in the database
    */
    function deletePostIdQuery($entry_id, $slug) {
        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = 'DELETE FROM wp2h_cf_form_entry_values WHERE slug = ? AND entry_id = ?';
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $slug);
            $stmt->bindValue(2, $entry_id);
            $stmt->execute();
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

    /**
    * Refreshes the page and remove an event that has been approved/deleted from the displayed list of "new events"
    */
    function refreshPage() {
        echo "<script type\"text/javascript\">
            window.onload = function() {
                if(!window.location.hash) {
                    window.location = window.location + '#modificado';
                    window.location.reload();
                }
            }
            removeHash();
            function removeHash () { 
                var scrollV, scrollH, loc = window.location;
                if (\"pushState\" in history)
                    history.pushState(\"\", document.title, loc.pathname + loc.search);
                else {
                    // Prevent scrolling by storing the page's current scroll offset
                    scrollV = document.body.scrollTop;
                    scrollH = document.body.scrollLeft;

                    loc.hash = \"\";

                    // Restore the scroll offset, should be flicker free
                    document.body.scrollTop = scrollV;
                    document.body.scrollLeft = scrollH;
                }
            }
            </script>";
    }

    /**
    * Builds all of the input fields
    */
    function loadInputs() {
        ?>
        <h2>Editar los eventos</h2>
        <!-- The box of editable elements -->
        <form method="post" onsubmit="return confirmApprove()">
            <div style="border: 1px solid #AAAAAA; border-radius: 7px; padding: 15px; margin-bottom: 50px">
                <div style="display: flex">
                    <div style="flex-grow: 1; margin-right: 20px">
                        <label>Nombre del evento</label>
                        <input type="text" name="i_event_name" id="i_event_name" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-left: 20px">
                        <label>Dirección del evento</label>
                        <input type="text" name="i_event_address" id="i_event_address" disabled>
                    </div>
                </div>

                <div style="display: flex">
                    <div style="flex-grow: 1; margin-right: 20px">
                        <label>Fecha de inicio</label>
                        <input type="text" name="i_start_date" id="i_start_date" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-left: 20px; margin-right: 20px">
                        <label>Fecha de finalización</label>
                        <input type="text" name="i_end_date" id="i_end_date" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-left: 20px; margin-right: 20px">
                        <label>Hora de inicio</label>
                        <input type="text" name="i_start_time" id="i_start_time" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-left: 20px; margin-right: 20px">
                        <label>Hora de finalización</label>
                        <input type="text" name="i_end_time" id="i_end_time" disabled>
                    </div>
                </div>

                <div style="display: flex">
                    <div style="flex-grow: 1; margin-right: 20px">
                        <label>Nombre de contacto</label>
                        <input type="text" name="i_contact_name" id="i_contact_name" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-let: 20px; margin-right: 20px">
                        <label>Email de contacto</label>
                        <input type="text" name="i_contact_email" id="i_contact_email" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-left: 20px">
                        <label>Teléfono de contacto</label>
                        <input type="text" name="i_contact_phone" id="i_contact_phone" disabled>
                    </div>
                </div>

                <div>
                    <label>Descripción (francés)</label><br>
                    <textarea rows="5" style="border: none; border-radius: 1px; width: 100%" name="i_event_description_french" id="i_event_description_french" readonly></textarea>
                </div>
                <div>
                    <label>Descripción (español)</label><br>
                    <textarea rows="5" style="border: none; border-radius: 1px; width: 100%" name="i_event_description_spanish" id="i_event_description_spanish" readonly></textarea>
                </div>
                <div>
                    <label>Descripción (inglés)</label><br>
                    <textarea rows="5" style="border: none; border-radius: 1px; width: 100%" name="i_event_description_english" id="i_event_description_english" readonly></textarea>
                </div>
                <div>
                    <label>Notas</label>
                    <textarea rows="5" style="border: none; border-radius: 1px; width: 100%" name="i_notes" id="i_notes" readonly></textarea>
                </div>

                <div style="display: flex">
                    <div style="flex-grow: 2; margin-right: 20px">
                        <label>Público objetivo</label>
                        <input type="text" name="i_target_audience" id="i_target_audience" disabled>
                    </div>
                    <div style="flex-grow: 1; margin-right: 20px; margin-left: 20px">
                        <label>Precio</label>
                        <input type="text" name="i_price" id="i_price" disabled>
                    </div>
                    <div style="flex-grow: 10; margin-right: 20px; margin-left: 20px">
                        <label>Imagen</label>
                        <input type="text" name="i_event_brochure" id="i_event_brochure" disabled>
                    </div>
                    <input type="hidden" id="i_entry_id" name="i_entry_id">
                    <input type="hidden" id="i_column" name="i_column">
                    <input type="hidden" id="i_postid_en" name="i_postid_en">
                    <input type="hidden" id="i_postid_es" name="i_postid_es">
                    <input type="hidden" id="i_postid_fr" name="i_postid_fr">
                </div>
                <div style="margin-top: 10px">
                    <center><img style="border-radius: 7px 40px 7px" height="150px" width="150px" src="https://moisheritagelatinoamericain.ca/wp-content/uploads/2020/default/default_brochure.jpg" id="i_event_brochure_img"></center>
                </div>
                
                <div style="text-align: center; display: flex; align-items: center; justify-content: center; margin: 1%;">
                    <input style="height: 40px; padding: 10px 25px 10px 25px; font-size: 18px" type="submit" name="approve" value="Aprobar">
                </div>
            </div>
        </form>

        <?php
    }

    /**
    * Builds all of the events in the database that are in the current year, the next year, and the following year.
    */
    function loadEvents($events, $value, $header) {
        echo "<h3>".$header."</h3>";
        // Generates the unapproved events
        $counter = 0;
        foreach ($events as $event) {
            
            // Gets the current year
            $currentYear = $datetoday = date('Y');
            $eventYear = substr($event['start_date'], 0, 4);
            
            $isOfCurrentYear = false;
            $isOneYearInAdvance = false;
            $isTwoYearsInAdvance = false;
            
            if ($eventYear == $currentYear) {
                $isOfCurrentYear = true;
            } else if (($eventYear - $currentYear) == 1) {
                $isOneYearInAdvance = true;
            } else if (($eventYear - $currentYear) == 2) {
                $isTwoYearsInAdvance = true;
            }
            
            if ($event['approved'] == $value && ($isOfCurrentYear || $isOneYearInAdvance || $isTwoYearsInAdvance)) {
                $counter += 1;
                echo "<div>";
                echo "<div>";
                // Generates image whether it is in the database or the default
                $image_url = "https://moisheritagelatinoamericain.ca/wp-content/uploads/2020/default/default_brochure.jpg";
                if ($event['event_brochure'] != "") {
                    $image_url = $event['event_brochure'];
                }
                echo "<img style='height: 100px; width: 100px; margin-right: 25px; inline-block; vertical-align: top; border-radius: 3px 25px 3px' src='".$image_url."'"; 
                echo "</div>";

                echo "<div style='display: inline-block'>";
                echo "<strong>".$event['event_name']."</strong></br>";
                echo $event['event_address']."</br>";
                if (isset($event['end_date'])) {
                    echo $event['start_date']." a ".$event['end_date']." de ".$event['start_time']." a ".$event['end_time']."</br>";
                } else {
                    echo $event['start_date']." de ".$event['start_time']." a ".$event['end_time']."</br>";
                }
                ?>

                <!-- Edit button that fills the fields above with the respective data -->
                <div style="display: flex">
                    <?php $event['column'] = $value; ?>
                    <input id="editEventButton" style="flex-grow: 1; margin-right: 10px" type="button" onclick="editEvent(<?=htmlentities(json_encode($event))?>);" name="editEventButton" value="Editar<?php if ($value == 0) { echo ' y Aprobar'; } ?>">

                    <!-- Deletes the event if the deletion is confirmed -->
                    <?php
                    if ($value == 0) { ?>
                    <form style="flex-grow: 1" method="post" onsubmit="return confirmDelete()">
                        <input type="submit" name="delete<?php echo $event['entry_id'] ?>" value="Eliminar">
                    </form>
                    <?php
                    }
                    else {
                        ?>
                    <form style="flex-grow: 1" method="post" onsubmit="return confirmUnapprove()">
                        <input type="hidden" name="un_post_id_en" value="<?php $event['post_id_en'] ?>">
                        <input type="hidden" name="un_post_id_es" value="<?php $event['post_id_es'] ?>">
                        <input type="hidden" name="un_post_id_fr" value="<?php $event['post_id_fr'] ?>">
                        
                        <input type="submit" name="unapprove<?php echo $event['entry_id'] ?>" value="Desaprobar">
                    </form>
                    <?php
                    } ?>
                </div>

                <?php
                echo "</div></div>";
                
                // If delete for that particular event was pressed
                // Deletes the event from the database
                if (isset($_POST['delete'.$event['entry_id']])) {
                    deleteEventQuery($event);
                    refreshPage();
                    echo "<script>alert('El evento ha sido eliminado');</script>";
                    unset($_POST['delete'.$event['entry_id']]);
                }
                
                // If the unapprove button for that particular event was pressed
                // Deletes the event from the posts, unapproves in the database
                if (isset($_POST['unapprove'.$event['entry_id']])) {
                    deletePost($event);
                    unapproveEventQuery($event['entry_id']);
                    refreshPage();
                    echo "<script>alert('El evento ha sido desaprobado');</script>";
                    unset($_POST['unapprove'.$event['entry_id']]);
                }
                echo "</div>";
            }
            // If an event was approved and its date is changed too far in the future, do not display it
            else if ($event['approved'] == $value && ($eventYear - $currentYear) > 2) {
                unapproveEventQuery($event['entry_id']);
            }
        } if ($counter == 0) {
            echo "No hay ".strtolower($header);
        }
    }

    /**
    * Approves the event in the database and updates all fields in the database
    */
    function approveEvent($value) {
        if ($value == 0) {
            $isCancelled = createEventPost();
            if ($isCancelled) {
                unset($_POST['approve']);
                return true;
            }
        } else {
            editEventPost();
        }
        updateFieldQuery("event_name", $_POST['i_entry_id'], $_POST['i_event_name']);
        updateFieldQuery("event_address", $_POST['i_entry_id'], $_POST['i_event_address']);
        updateFieldQuery("start_date", $_POST['i_entry_id'], $_POST['i_start_date']);
        updateFieldQuery("end_date", $_POST['i_entry_id'], $_POST['i_end_date']);
        updateFieldQuery("start_time", $_POST['i_entry_id'], $_POST['i_start_time']);
        updateFieldQuery("end_time", $_POST['i_entry_id'], $_POST['i_end_time']);
        updateFieldQuery("contact_name", $_POST['i_entry_id'], $_POST['i_contact_name']);
        updateFieldQuery("contact_email", $_POST['i_entry_id'], $_POST['i_contact_email']);
        updateFieldQuery("contact_phone", $_POST['i_entry_id'], $_POST['i_contact_phone']);
        
        $french = fixSpacing($_POST['i_event_description_french']);
        $english = fixSpacing($_POST['i_event_description_english']);
        $spanish = fixSpacing($_POST['i_event_description_spanish']);
        $notes = fixSpacing($_POST['i_notes']);
        
        updateFieldQuery("event_description_french", $_POST['i_entry_id'], $french);
        updateFieldQuery("event_description_spanish", $_POST['i_entry_id'], $spanish);
        updateFieldQuery("event_description_english", $_POST['i_entry_id'], $english);
        updateFieldQuery("target_audience", $_POST['i_entry_id'], $_POST['i_target_audience']);
        updateFieldQuery("price", $_POST['i_entry_id'], $_POST['i_price']);
        updateFieldQuery("event_brochure", $_POST['entry_id'], $_POST['i_event_brochure']);
        updateFieldQuery("notes", $_POST['i_entry_id'], $notes);
        approveEventQuery($_POST['i_entry_id']);
        
        refreshPage();
        echo "<script type=\"text/javascript\">alert('El evento ha sido aprobado/modificado');</script>";
        unset($_POST['approve']);
    }

    /**
    * Replaces all strange newline characters, as well as non-breaking spaces with
    * regular <br> and space characters
    */
    function fixSpacing($description) {
        $rn = str_replace("\r\n", "<br>", $description);
        $nr = str_replace("\n\r", "<br>", $rn);
        $r = str_replace("\r", "<br>", $nr);
        $n = str_replace("\n", "<br>", $r);
        $space = str_replace(" ", " ", $n);
        $nbsp = str_replace("&nbsp;", " ", $space);
        return $nbsp;
    }

    /**
    * Creates a new post based on the input stored in the event fields
    */
    function createEventPost() {
        if ($_POST['i_event_name'] == "") {
            echo "<script>alert('¡Debes seleccionar un evento!');</script>";
            return true;
        }
        
        echo "*".$_POST['i_price']."*";
        
        $image_div = "<div style=\"display:inline-block; vertical-align: top\">
            <img style=\"border-radius: 7px 40px 7px\" height=\"200px\" width=\"200px\" src=\"".$_POST['i_event_brochure']."\"/>
        </div>";
        
        // English date settings
        setlocale(LC_TIME, "en_CA");
        $format = "%B %e, %Y";
        $start_date_en_format = DateTime::createFromFormat('Y-m-d', $_POST['i_start_date']);
        $timestamp_start_en = strtotime($start_date_en_format->format("m/d/Y"));
        $start_date_en = strftime($format, $timestamp_start_en);
        $end_date_en;
        $date_en;
        $target_audience_en;
        $price_en;
        if ($_POST['i_target_audience'] == "Everyone") {
            $target_audience_en = "Everyone!";
        } else if ($_POST['i_target_audience'] == "Children") {
            $target_audience_en = "Children";
        } else if ($_POST['i_target_audience'] == "Adults") {
            $target_audience_en = "Adults";
        }
        if ($_POST['i_price'] == "0.00") {
            $price_en = "FREE!";
        } else {
            $price_en = "$".$_POST['i_price'];
        }
        if (isset($_POST['i_end_date']) && $_POST['i_end_date'] != "") {
            $end_date_en_format = DateTime::createFromFormat('Y-m-d', $_POST['i_end_date']);
            $timestamp_end_en = strtotime($end_date_en_format->format("m/d/Y"));
            $end_date_en = strftime($format, $timestamp_end_en);
            $date_en = $start_date_en." to ".$end_date_en;
        } else {
            $date_en = $start_date_en;
        }
        
        // Spanish date settings
        setlocale(LC_TIME, "es_ES");
        $format = "%e %B, %Y";
        $start_date_es_format = DateTime::createFromFormat('Y-m-d', $_POST['i_start_date']);
        $timestamp_start_es = strtotime($start_date_es_format->format("m/d/Y"));
        $start_date_es = strftime($format, $timestamp_start_es);
        $end_date_es;
        $date_es;
        $target_audience_es;
        price_es;
        if ($_POST['i_target_audience'] == "Everyone") {
            $target_audience_es = "Todos!";
        } else if ($_POST['i_target_audience'] == "Children") {
            $target_audience_es = "Niños";
        } else if ($_POST['i_target_audience'] == "Adults") {
            $target_audience_es = "Adultos";
        }
        if ($_POST['i_price'] == "0.00") {
            $price_es = "GRATIS!";
        } else {
            $price_es = "$".$_POST['i_price'];
        }
        if (isset($_POST['i_end_date']) && $_POST['i_end_date'] != "") {
            $end_date_es_format = DateTime::createFromFormat('Y-m-d', $_POST['i_end_date']);
            $timestamp_end_es = strtotime($end_date_es_format->format("m/d/Y"));
            $end_date_es = strftime($format, $timestamp_end_es);
            $date_es = $start_date_es." a ".$end_date_es;
        } else {
            $date_es = $start_date_es;
        }
        
        // French date settings
        setlocale(LC_TIME, "fr_CA");
        $format = "%e %B, %Y";
        $start_date_fr_format = DateTime::createFromFormat('Y-m-d', $_POST['i_start_date']);
        $timestamp_start_fr = strtotime($start_date_fr_format->format("m/d/Y"));
        $start_date_fr = strftime($format, $timestamp_start_fr);
        $end_date_fr;
        $date_fr;
        $target_audience_fr;
        price_fr;
        if ($_POST['i_target_audience'] == "Everyone") {
            $target_audience_fr = "Tous!";
        } else if ($_POST['i_target_audience'] == "Children") {
            $target_audience_fr = "Enfants";
        } else if ($_POST['i_target_audience'] == "Adults") {
            $target_audience_fr = "Adultes";
        }
        if ($_POST['i_price'] == "0.00") {
            $price_fr = "GRATUIT!";
        } else {
            $price_fr = "$".$_POST['i_price'];
        }
        if (isset($_POST['i_end_date']) && $_POST['i_end_date'] != "") {
            $end_date_fr_format = DateTime::createFromFormat('Y-m-d', $_POST['i_end_date']);
            $timestamp_end_fr = strtotime($end_date_fr_format->format("m/d/Y"));
            $end_date_fr = strftime($format, $timestamp_end_fr);
            $date_fr = $start_date_fr." à ".$end_date_fr;
        } else {
            $date_fr = $start_date_fr;
        }
        
        // English details div
        $details_div_en = "<div style=\"display:inline-block; margin-left: 30px; margin-top: 15px; line-height: 24pt; font-size: 16px\">
            <strong>Event Name: </strong>".$_POST['i_event_name']."<br>
            <strong>Event Address: </strong>".$_POST['i_event_address']."<br>
            <strong>Date: </strong>".$date_en."<br>
            <strong>Time: </strong>".$_POST['i_start_time']." - ".$_POST['i_end_time']."<br>
            <strong>Target Audience: </strong>".$target_audience_en."<br>
            <strong>Price: </strong>".$price_en."
        </div>";
        
        // Spanish details div
        $details_div_es = "<div style=\"display:inline-block; margin-left: 30px; margin-top: 15px; line-height: 24pt; font-size: 16px\">
            <strong>Nombre del evento: </strong>".$_POST['i_event_name']."<br>
            <strong>Dirección del evento: </strong>".$_POST['i_event_address']."<br>
            <strong>Fecha: </strong>".$date_es."<br>
            <strong>Hora: </strong>".$_POST['i_start_time']." - ".$_POST['i_end_time']."<br>
            <strong>Público objetivo: </strong>".$target_audience_es."<br>
            <strong>Precio: </strong>".$price_es."
        </div>";
        
        // French details div
        $details_div_fr = "<div style=\"display:inline-block; margin-left: 30px; margin-top: 15px; line-height: 24pt; font-size: 16px\">
            <strong>Nom de l'événement: </strong>".$_POST['i_event_name']."<br>
            <strong>Adresse de l'événement: </strong>".$_POST['i_event_address']."<br>
            <strong>Date: </strong>".$date_fr."<br>
            <strong>Heure: </strong>".$_POST['i_start_time']." - ".$_POST['i_end_time']."<br>
            <strong>Public cible: </strong>".$target_audience_fr."<br>
            <strong>Prix de l'événement: </strong>".$price_fr."
        </div>";
        
        $description_div_en = "<h4 style=\"margin-top: 20px\">Event Description</h4><p style=\"font-size: 16px\">
            ".fixSpacing($_POST['i_event_description_english'])."
        </p>";
        
        $description_div_es = "<h4 style=\"margin-top: 20px\">Descripción del evento</h4><p style=\"font-size: 16px\">
            ".fixSpacing($_POST['i_event_description_spanish'])."
        </p>";
        
        $description_div_fr = "<h4 style=\"margin-top: 20px\">Description de l'événement</h4><p style=\"font-size: 16px\">
            ".fixSpacing($_POST['i_event_description_french'])."
        </p>";
        
        $content_en = $image_div.$details_div_en.$description_div_en;
        $content_es = $image_div.$details_div_es.$description_div_es;
        $content_fr = $image_div.$details_div_fr.$description_div_fr;
        
        $event_post_en = array(
            'post_title' => $_POST['i_event_name'],
            'post_content' => $content_en,
            'post_status' => 'publish',
            'post_author' => 3,
            'post_category' => array(96)
        );

        $event_post_es = array(
            'post_title' => $_POST['i_event_name'],
            'post_content' => $content_es,
            'post_status' => 'publish',
            'post_author' => 3,
            'post_category' => array(112)
        );

        $event_post_fr = array(
            'post_title' => $_POST['i_event_name'],
            'post_content' => $content_fr,
            'post_status' => 'publish',
            'post_author' => 3,
            'post_category' => array(1)
        );

        /* Sourced from: https://wordpress.stackexchange.com/questions/224278/polylang-translation-of-a-custom-post-created-by-wp-insert-post */
        if ($id = wp_insert_post($event_post_en, true)) {
            $posts = array(
                'es' => '',
                'fr' => '',
            );

        // Creates post for each language, sets id
        $posts['es'] = wp_insert_post($event_post_es, true);
        $posts['fr'] = wp_insert_post($event_post_fr, true);

            // Sets as translations of the same post
            if (function_exists('pll_set_post_language') 
                && function_exists('pll_save_post_translations')) {

                pll_set_post_language($id, 'en');
                foreach ($posts as $language => $value) {
                    pll_set_post_language($value, $language);
                    // Add each post id to the database
                    addPostIdQuery($value, $language);
                }
                addPostIdQuery($id, 'en');

                pll_save_post_translations(array_merge(array($id), $posts));
            }
        }
	}

    /**
    * Edits an existing post based on the input stored in the event fields
    */
    function editEventPost() {
        $image_div = "<div style=\"display:inline-block; vertical-align: top\">
            <img style=\"border-radius: 7px 40px 7px\" height=\"200px\" width=\"200px\" src=\"".$_POST['i_event_brochure']."\"/>
        </div>";
        
        // English date settings
        setlocale(LC_TIME, "en_CA");
        $format = "%B %e, %Y";
        $start_date_en_format = DateTime::createFromFormat('Y-m-d', $_POST['i_start_date']);
        $timestamp_start_en = strtotime($start_date_en_format->format("m/d/Y"));
        $start_date_en = strftime($format, $timestamp_start_en);
        $end_date_en;
        $date_en;
        $target_audience_en;
        $price_en;
        if ($_POST['i_target_audience'] == "Everyone") {
            $target_audience_en = "Everyone!";
        } else if ($_POST['i_target_audience'] == "Children") {
            $target_audience_en = "Children";
        } else if ($_POST['i_target_audience'] == "Adults") {
            $target_audience_en = "Adults";
        }
        if ($_POST['i_price'] == "0.00") {
            $price_en = "FREE!";
        } else {
            $price_en = "$".$_POST['i_price'];
        }
        if (isset($_POST['i_end_date']) && $_POST['i_end_date'] != "") {
            $end_date_en_format = DateTime::createFromFormat('Y-m-d', $_POST['i_end_date']);
            $timestamp_end_en = strtotime($end_date_en_format->format("m/d/Y"));
            $end_date_en = strftime($format, $timestamp_end_en);
            $date_en = $start_date_en." to ".$end_date_en;
        } else {
            $date_en = $start_date_en;
        }
        
        // Spanish date settings
        setlocale(LC_TIME, "es_ES");
        $format = "%e %B, %Y";
        $start_date_es_format = DateTime::createFromFormat('Y-m-d', $_POST['i_start_date']);
        $timestamp_start_es = strtotime($start_date_es_format->format("m/d/Y"));
        $start_date_es = strftime($format, $timestamp_start_es);
        $end_date_es;
        $date_es;
        $target_audience_es;
        price_es;
        if ($_POST['i_target_audience'] == "Everyone") {
            $target_audience_es = "Todos!";
        } else if ($_POST['i_target_audience'] == "Children") {
            $target_audience_es = "Niños";
        } else if ($_POST['i_target_audience'] == "Adults") {
            $target_audience_es = "Adultos";
        }
        if ($_POST['i_price'] == "0.00") {
            $price_es = "GRATIS!";
        } else {
            $price_es = "$".$_POST['i_price'];
        }
        if (isset($_POST['i_end_date']) && $_POST['i_end_date'] != "") {
            $end_date_es_format = DateTime::createFromFormat('Y-m-d', $_POST['i_end_date']);
            $timestamp_end_es = strtotime($end_date_es_format->format("m/d/Y"));
            $end_date_es = strftime($format, $timestamp_end_es);
            $date_es = $start_date_es." a ".$end_date_es;
        } else {
            $date_es = $start_date_es;
        }
        
        // French date settings
        setlocale(LC_TIME, "fr_CA");
        $format = "%e %B, %Y";
        $start_date_fr_format = DateTime::createFromFormat('Y-m-d', $_POST['i_start_date']);
        $timestamp_start_fr = strtotime($start_date_fr_format->format("m/d/Y"));
        $start_date_fr = strftime($format, $timestamp_start_fr);
        $end_date_fr;
        $date_fr;
        $target_audience_fr;
        price_fr;
        if ($_POST['i_target_audience'] == "Everyone") {
            $target_audience_fr = "Tous!";
        } else if ($_POST['i_target_audience'] == "Children") {
            $target_audience_fr = "Enfants";
        } else if ($_POST['i_target_audience'] == "Adults") {
            $target_audience_fr = "Adultes";
        }
        if ($_POST['i_price'] == "0.00") {
            $price_fr = "GRATUIT!";
        } else {
            $price_fr = "$".$_POST['i_price'];
        }
        if (isset($_POST['i_end_date']) && $_POST['i_end_date'] != "") {
            $end_date_fr_format = DateTime::createFromFormat('Y-m-d', $_POST['i_end_date']);
            $timestamp_end_fr = strtotime($end_date_fr_format->format("m/d/Y"));
            $end_date_fr = strftime($format, $timestamp_end_fr);
            $date_fr = $start_date_fr." à ".$end_date_fr;
        } else {
            $date_fr = $start_date_fr;
        }
        
        // English details div
        $details_div_en = "<div style=\"display:inline-block; margin-left: 30px; margin-top: 15px; line-height: 24pt; font-size: 16px\">
            <strong>Event Name: </strong>".$_POST['i_event_name']."<br>
            <strong>Event Address: </strong>".$_POST['i_event_address']."<br>
            <strong>Date: </strong>".$date_en."<br>
            <strong>Time: </strong>".$_POST['i_start_time']." - ".$_POST['i_end_time']."<br>
            <strong>Target Audience: </strong>".$target_audience_en."<br>
            <strong>Price: </strong>".$price_en."
        </div>";
        
        // Spanish details div
        $details_div_es = "<div style=\"display:inline-block; margin-left: 30px; margin-top: 15px; line-height: 24pt; font-size: 16px\">
            <strong>Nombre del evento: </strong>".$_POST['i_event_name']."<br>
            <strong>Dirección del evento: </strong>".$_POST['i_event_address']."<br>
            <strong>Fecha: </strong>".$date_es."<br>
            <strong>Hora: </strong>".$_POST['i_start_time']." - ".$_POST['i_end_time']."<br>
            <strong>Público objetivo: </strong>".$target_audience_es."<br>
            <strong>Precio: </strong>".$price_es."
        </div>";
        
        // French details div
        $details_div_fr = "<div style=\"display:inline-block; margin-left: 30px; margin-top: 15px; line-height: 24pt; font-size: 16px\">
            <strong>Nom de l'événement: </strong>".$_POST['i_event_name']."<br>
            <strong>Adresse de l'événement: </strong>".$_POST['i_event_address']."<br>
            <strong>Date: </strong>".$date_fr."<br>
            <strong>Heure: </strong>".$_POST['i_start_time']." - ".$_POST['i_end_time']."<br>
            <strong>Public cible: </strong>".$target_audience_fr."<br>
            <strong>Prix de l'événement: </strong>".$price_fr."
        </div>";
        
        $description_div_en = "<h4 style=\"margin-top: 20px\">Event Description</h4><p style=\"font-size: 16px\">
            ".fixSpacing($_POST['i_event_description_english'])."
        </p>";
        
        $description_div_es = "<h4 style=\"margin-top: 20px\">Descripción del evento</h4><p style=\"font-size: 16px\">
            ".fixSpacing($_POST['i_event_description_spanish'])."
        </p>";
        
        $description_div_fr = "<h4 style=\"margin-top: 20px\">Description de l'événement</h4><p style=\"font-size: 16px\">
            ".fixSpacing($_POST['i_event_description_french'])."
        </p>";
        
        $content_en = $image_div.$details_div_en.$description_div_en;
        $content_es = $image_div.$details_div_es.$description_div_es;
        $content_fr = $image_div.$details_div_fr.$description_div_fr;
        
        $event_post_en = array(
            'ID' => $_POST['i_postid_en'],
            'post_title' => $_POST['i_event_name'],
            'post_content' => $content_en
        );

        $event_post_es = array(
            'ID' => $_POST['i_postid_es'],
            'post_title' => $_POST['i_event_name'],
            'post_content' => $content_es
        );

        $event_post_fr = array(
            'ID' => $_POST['i_postid_fr'],
            'post_title' => $_POST['i_event_name'],
            'post_content' => $content_fr
        );
        
        wp_update_post($event_post_en);
        wp_update_post($event_post_es);
        wp_update_post($event_post_fr);
    }

    /**
    * Deletes a post from Wordpress, and deletes the post id references
    */
    function deletePost($event) {
        wp_delete_post($event['post_id_en']);
        wp_delete_post($event['post_id_es']);
        wp_delete_post($event['post_id_fr']);
        deletePostIdQuery($event['entry_id'], post_id_en);
        deletePostIdQuery($event['entry_id'], post_id_es);
        deletePostIdQuery($event['entry_id'], post_id_fr);
    }

?>
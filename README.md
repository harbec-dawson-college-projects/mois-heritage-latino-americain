As a part of Dawson College's Computer Science program, I spent my internship
working on the improvement of Mois Héritage Latino-Américain (MHLA)'s website.

As the website was on the free version of Wordpress, I came up with the alternative of using independent PHP scripts to implement our desired features through the plugin XYZ PHP Code Snippets. Because of this, many of the scripts are much longer than they could be, however they all needed to be independent.

My tasks revolved around improving the state of their database, and more notably
add a simpler way to manage and publish events that are submitted by users, as
well as making these events display across different parts of the website. I
additionally helped my classmate (and co-worker) with the visual styling and
JavaScript functionality of a calendar which contained the published events.

These independent scripts were used as snippets through the XYZ PHP Snippet Code
plugin for Wordpress. As there was no need to support additional languages in the future, internationalization was not implemented and much of the website was hardcoded. I have also removed the initialization of the variables that connect to the database for confidentiality.

The website can be accessed at: http://moisheritagelatinoamericain.ca/
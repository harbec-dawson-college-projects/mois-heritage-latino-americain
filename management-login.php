<?php
/**
* The management-login script is used to create a login form, and verify that the credentials are valid.
* Proper password protection has been implemented.
*
* @author Liam Harbec, 2020, liam.harbec13@gmail.com
*/
?>

<html>
    <head>
        <style type="text/css">
            .center {
                text-align: center;
                display: flex;
                align-items: center;
                justify-content: center;
                margin: 1%;
            }
        </style>
    </head>
    <body>
        <form method="post">
                <div class="center">
                    <h5>Nombre de usario&nbsp;&nbsp;</h5>
                    <input style="width: 255px" type="text" name="username"></br></br>
                </div>
                <div class="center">
                    <h5>Contraseña&nbsp;&nbsp;</h5>
                    <input style="width: 300px" type="password" name="password"></br></br>
                </div>
                <div class="center">
                    <input type="submit" name="submit" value="Iniciar sesión">
                </div>
        </form>
    </body>
</html>

<?php
    if(isset($_POST['submit'])) {
        $input_username = $_POST['username'];
        $input_password = $_POST['password'];

        // Assume $name, $host, $user, and $password have been properly assigned
        try {
            $pdo = new PDO("mysql:dbname=$name;host=$host", "$user", "$password");
            $query = "SELECT * FROM users WHERE username = ?";
            $stmt = $pdo->prepare($query);
            $stmt->bindValue(1, $input_username);
            $stmt->execute();
            $result = $stmt->fetch();

            $retrieved_salt = $result['salt'];
            $retrieved_password = $result['password'];
			$id = session_id();
            // If the password entered matches the one in the database
            if (password_verify($input_password.$retrieved_salt, $retrieved_password)) {
                // If the login was successful
				if(isset($_SESSION['manager_login']) && $_SESSION['manager_login']){
					session_start();
					if(strcmp($_SESSION['id'], $id) != 0){
						setcookie(session_id($_SESSION['id']),'', time() - 42000);
						$_SESSION=[];
					}
				}
				$_SESSION['id'] = $id;
                $_SESSION['manager_login'] = true;
                $url = "https://moisheritagelatinoamericain.ca/es/gestionar";
                echo("<script>location.href = '".$url."'</script>");	
            } else {
                echo "<p class=\"center\" style=\"color: red\">Esas credenciales no son válidas. Inténtalo de nuevo.</p>";
            }
        } catch (PDOException $e) {
            echo 'Please contact the website administrators: \n\n'.$e->getMessage();
        } finally {
            unset($pdo);
        }
    }

?>
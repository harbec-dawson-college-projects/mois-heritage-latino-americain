<?php
/**
* The all-events script is used to display all approved events that have not yet passed.
* They are separated by category, being "Everyone" and "Adults".
* It is adapted to be internationalized.
*
* @author Liam Harbec, 2020, liam.harbec13@gmail.com
*/
?>

<style type="text/css">
    
    .all_upcoming {
        padding: 10px;
        border: solid 1px transparent;
        width: 90%;
        min-height: 120px;
    }
    
    .all_upcoming:hover {
        border: solid 1px #BEBEBE;
        border-radius: 5px;
        background-color: whitesmoke;
        -webkit-box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
        -moz-box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
        box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
    }
    
    .all_event_image {
        height: 100px;
        width: 100px;
        display: inline-block;
        vertical-align: top;
        margin-right: 10px;
        border-radius: 3px 25px 3px;
    }
    
    @media screen and (max-width: 1000px) {
        .upcoming {
            width: 100%;
        }
    }
    
</style>

<br><br>
<?php
    $current_locale = pll_current_language('locale');
    if (isset($_SESSION['events'])) {
        $events = $_SESSION['events'];

        $events_not_passed = get_events_not_passed($events);
        asort($events_not_passed);
        
        $keys = array_keys($events_not_passed);
        if (!empty($keys)) {
            echo "<div style='display: flex; margin: 0 5%'>";
            echo "<div style='width: 50%'>";
            
            $family_header;
            $adults_header;
            if ($current_locale == 'en_CA') {
                $family_header = "Family";
                $adults_header = "Adults";
            } else if ($current_locale == 'es_ES') {
                $family_header = "Familia";
                $adults_header = "Adultos";
            } else if ($current_locale == 'fr_CA') {
                $family_header = "Famille";
                $adults_header = "Adultes";
            } else {
                $family_header = "Family";
                $adults_header = "Adults";
            }
            
            echo "<center><u><h2>$family_header</h2></u></center>";
            get_events($events, $keys, "Children");
            echo "</div>";
            
            echo "<div style='width: 50%'>";
            echo "<center><u><h2>$adults_header</h2></u></center>";
            get_events($events, $keys, "Adults");
            echo "</div>";
            echo "</div>";
            
            echo "<br><br>";
        }
    } else {
        if ($current_locale == 'en_CA') {
            echo "There are no upcoming events.";
        } else if ($current_locale == 'es_ES') {
            echo "No hay eventos próximos.";
        } else if ($current_locale == 'fr_CA') {
            echo "Il n'y a aucun évènement à venir.";
        } else {
            echo "There are no upcoming events.";
        }
    }

    /**
    * Gets all events that have not yet passed
    */
    function get_events_not_passed($events) {
        $allEvents = array();
        // Adds all events as id => start_date
        foreach ($events as $key => $event) {
            // Creating the TimeZone object
            date_default_timezone_set('America/Toronto');
            $timezone = new DateTimeZone('America/Toronto');

            // If the event has already passed, do not display
            try {
                $eventAsDateTime = new DateTime($event['start_date']." ".$event['start_time'].":00");
                $eventAsDateTime->setTimezone($timezone);
                $currentDate = new DateTime();
                $currentDate->setTimezone($timezone);

                if ($event['approved'] == 1 && $eventAsDateTime > $currentDate) {
                    $allEvents[$key] = $event['start_date'];
                }
            }
            // Invalid date entered, cannot be parsed
            catch (Exception $e) {
                continue;
            }
        }
        // Sorts by start_date field (earliest to latest in time)
        return $allEvents;
    }

    function get_events($events, $keys, $category) {
        $counter = 0;
        foreach ($keys as $key) {
            if ($counter == 10) {
                break;
            }
            $found_event = $events[$key];
            $isSuccess = calculate_if_passed($found_event, $counter, $category);
            if ($isSuccess) {
                $counter++;
            }
        }
        if ($counter == 0) {
            $current_locale = pll_current_language('locale');
            echo "<center>";
            if ($current_locale == 'en_CA') {
                if ($category == "Children") {
                    echo "There are no events for children.";
                } else if ($category == "Adults") {
                    echo "There are no events for adults.";
                }
            } else if ($current_locale == 'es_ES') {
                if ($category == "Children") {
                    echo "No hay eventos para niños.";
                } else if ($category == "Adults") {
                    echo "No hay eventos para adultos.";
                }
            } else if ($current_locale == 'fr_CA') {
                if ($category == "Children") {
                    echo "Il n'y a pas d'événements pour les enfants.";
                } else if ($category == "Adults") {
                    echo "Il n'y a pas d'événements pour les adultes.";
                }
            } else {
                if ($category == "Children") {
                    echo "There are no events for ".$category;
                } else if ($category == "Adults") {
                    echo "There are no events for ".$category;
                }
            }
            echo "</center>";
        }
    }

    function get_all_events($events, $keys) { 
        $counter = 0;
        echo "<div style='width: 50%'>";
        foreach ($f_half as $f_key) {
            if ($counter == 10) {
                break;
            }
            $found_event = $events[$f_key];
            $isSuccess = calculate_if_passed($found_event, $counter);
            if ($isSuccess) {
                $counter++;
            }
        }
        echo "</div>";
        
        $counter = 0;
        echo "<div style='width: 50%'>";
        foreach ($s_half as $s_key) {
            if ($counter == 10) {
                break;
            }
            $found_event = $events[$s_key];
            $isSuccess = calculate_if_passed($found_event, $counter);
            if ($isSuccess) {
                $counter++;
            }
        }
        echo "</div>";
    }

    function calculate_if_passed($found_event, $counter, $category = null) {
        // Creating the TimeZone object
        date_default_timezone_set('America/Toronto');
        $timezone = new DateTimeZone('America/Toronto');

        // If the event has already passed, do not display
        $eventAsDateTime = new DateTime($found_event['start_date']." ".$found_event['start_time'].":00");
        $eventAsDateTime->setTimezone($timezone);
        $currentDate = new DateTime();
        $currentDate->setTimezone($timezone);

        // If the event has passed
        if ($eventAsDateTime <= $currentDate) {
            return false;
        }
        // If the event has not passed yet
        else {
            if ($found_event['target_audience'] == $category || ($category == "Children" && $found_event['target_audience'] == "Everyone") || $category == null) {
                display_event($found_event);
                return true;
            } else {
                return false;
            }
        }
    }

    function display_event($found_event) {
        $current_locale = pll_current_language('locale');
        $event_name = $found_event['event_name'];
        $retrieved_start_date = $found_event['start_date'];
        $start_date_format = DateTime::createFromFormat('Y-m-d', $retrieved_start_date);

        $start_time = $found_event['start_time'];
        $short_desc;
        $desc;
        $ft;
        $at;
        $post_id;
        if ($current_locale == 'en_CA') {
            $desc = $found_event['event_description_english'];
            setlocale(LC_TIME, "en_CA");
            $format = "%B %e, %Y";
            $at = " at ";
            $post_id = $found_event['post_id_en'];
        } else if ($current_locale == 'es_ES') {
            $desc = $found_event['event_description_spanish'];
            setlocale(LC_TIME, "es_ES");
            $format = "%e de %B, %Y";
            $at = " a ";
            $post_id = $found_event['post_id_es'];
        } else if ($current_locale == 'fr_CA') {
            $desc = $found_event['event_description_french'];
            setlocale(LC_TIME, "fr_CA");
            $format = "%e %B, %Y";
            $at = " à ";
            $post_id = $found_event['post_id_fr'];
        } else {
            $desc = $found_event['event_description_english'];
            setlocale(LC_TIME, "en");
            $format = "%e %B, %Y";
            $at = " at ";
            $post_id = $found_event['post_id_en'];
        }
        
        $noSpaces = str_replace(" ", " ", $desc);
        if (strlen($desc) > 130) {
            $short_desc = substr($noSpaces, 0, 130);
            if (rtrim($short_desc) != $short_desc) {
                $short_desc = rtrim($short_desc)."...";
            } else {
                $short_desc .= "...";
            }
        } else {
            $short_desc = $noSpaces;
        }

        $timestamp = strtotime($start_date_format->format("m/d/Y"));
        $start_date = strftime($format, $timestamp);
        $event_html = "<h5 style=\"margin-bottom: -25px\">";
        $event_html .= $event_name;
        $event_html .= "<i style=\"font-size: 12px; color: #AAAAAA; margin-top: -10px\"> (";
        $event_html .= $start_date;
        $event_html .= $at;
        $event_html .= $start_time;
        if (isset($found_event['end_time'])) {
            $event_html .= "-".$found_event['end_time'];
        }
        $event_html .= ")</i></h5></br>";
        $event_html .= "<p style=\"color: #6D6D6D\">".$short_desc."</p>";

        $image_url = "https://moisheritagelatinoamericain.ca/wp-content/uploads/2020/default/default_brochure.jpg";
        if ($found_event['event_brochure'] != "") {
            $image_url = $found_event['event_brochure'];
        }

        ?>
        <div class='all_upcoming'>
            <a href='https://moisheritagelatinoamericain.ca/en/home?p=<?php echo $post_id; ?>'>
                <img class="all_event_image" src="<?php echo $image_url ?>" align="left"><?php echo $event_html; ?>
            </a>
        </div>


        <?php
    }

?>
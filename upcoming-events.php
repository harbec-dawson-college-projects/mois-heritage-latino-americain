<?php
/**
* The upcoming-events script is used to display the 3 events that are soonest to occur.
* Once the event passes, it will no longer be displayed.
* It is adapted to be internationalized.
*
* @author Liam Harbec, 2020, liam.harbec13@gmail.com
*/
?>

<style type="text/css">
    .upcoming {
        padding: 10px;
        border: solid 1px transparent;
        width: 60%;
        min-height: 120px;
    }
    
    .upcoming:hover {
        border: solid 1px #BEBEBE;
        border-radius: 5px;
        background-color: whitesmoke;
        -webkit-box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
        -moz-box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
        box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
    }
    
    .event_image {
        height: 100px;
        width: 100px;
        display: inline-block;
        vertical-align: top;
        margin-right: 10px;
        border-radius: 3px 25px 3px;
    }
    
    @media screen and (max-width: 950px) {
        .upcoming {
            width: 100%;
        }
    }
    
</style>

<?php
    $current_locale = pll_current_language('locale');
    if (isset($_SESSION['events'])) {
        $events = $_SESSION['events'];

        $allEvents = array();
        // Adds all events as id => start_date
        foreach ($events as $key => $event) {
            
            // Creating the TimeZone object
            date_default_timezone_set('America/Toronto');
            $timezone = new DateTimeZone('America/Toronto');

            // If the event has already passed, do not display
            try {
                $eventAsDateTime = new DateTime($event['start_date']." ".$event['start_time'].":00");
                $eventAsDateTime->setTimezone($timezone);
                $currentDate = new DateTime();
                $currentDate->setTimezone($timezone);

                if ($event['approved'] == 1 && $eventAsDateTime > $currentDate) {
                    $allEvents[$key] = $event['start_date'];
                }
            }
            // Invalid date entered, cannot be parsed
            catch (Exception $e) {
                continue;
            }
            
        }
        // Sorts by start_date field (earliest to latest in time)
        asort($allEvents);

        $keys = array_keys($allEvents);
        $i = 0;
        if (empty($keys)) {
            if ($current_locale == 'en_CA') {
                echo "There are no upcoming events.";
            } else if ($current_locale == 'es_ES') {
                echo "No hay eventos próximos.";
            } else if ($current_locale == 'fr_CA') {
                echo "Il n'y a aucun évènement à venir.";
            } else {
                echo "There are no upcoming events.";
            }
        } else {
            foreach ($keys as $key) {
                if ($i < 3) {
                    $found_event = $events[$key];
                    
                    // Creating the TimeZone object
                    date_default_timezone_set('America/Toronto');
                    $timezone = new DateTimeZone('America/Toronto');
                    
                    // If the event has already passed, do not display
                    $eventAsDateTime = new DateTime($found_event['start_date']." ".$found_event['start_time'].":00");
                    $eventAsDateTime->setTimezone($timezone);
                    $currentDate = new DateTime();
                    $currentDate->setTimezone($timezone);

                    if ($eventAsDateTime <= $currentDate) {
                        continue;
                    }
                    // If the event has not passed yet
                    else {
                        $event_name = $found_event['event_name'];
                        $retrieved_start_date = $found_event['start_date'];
                        $start_date_format = DateTime::createFromFormat('Y-m-d', $retrieved_start_date);

                        $start_time = $found_event['start_time'];
                        $short_desc;
                        $desc;
                        $format;
                        $at;
                        $post_id;
                        if ($current_locale == 'en_CA') {
                            $desc = $found_event['event_description_english'];
                            setlocale(LC_TIME, "en_CA");
                            $format = "%B %e, %Y";
                            $at = " at ";
                            $post_id = $found_event['post_id_en'];
                        } else if ($current_locale == 'es_ES') {
                            $desc = $found_event['event_description_spanish'];
                            setlocale(LC_TIME, "es_ES");
                            $format = "%e de %B, %Y";
                            $at = " a ";
                            $post_id = $found_event['post_id_es'];
                        } else if ($current_locale == 'fr_CA') {
                            $desc = $found_event['event_description_french'];
                            setlocale(LC_TIME, "fr_CA");
                            $format = "%e %B, %Y";
                            $at = " à ";
                            $post_id = $found_event['post_id_fr'];
                        } else {
                            $desc = $found_event['event_description_english'];
                            setlocale(LC_TIME, "en");
                            $format = "%e %B, %Y";
                            $at = " at ";
                            $post_id = $found_event['post_id_en'];
                        }

                        $noSpaces = str_replace(" ", " ", $desc);
                        if (strlen($noSpaces) > 130) {
                            $short_desc = substr($noSpaces, 0, 130);
                            if (rtrim($short_desc) != $short_desc) {
                                $short_desc = rtrim($short_desc)."...";
                            } else {
                                $short_desc .= "...";
                            }
                        } else {
                            $short_desc = $noSpaces;
                        }

                        $timestamp = strtotime($start_date_format->format("m/d/Y"));
                        $start_date = strftime($format, $timestamp);
                        $event_html = "<h5 style=\"margin-bottom: -25px\">";
                        $event_html .= $event_name;
                        $event_html .= "<i style=\"font-size: 12px; color: #AAAAAA; margin-top: -10px\"> (";
                        $event_html .= $start_date;
                        $event_html .= $at;
                        $event_html .= $start_time;
                        if (isset($found_event['end_time'])) {
                            $event_html .= "-".$found_event['end_time'];
                        }
                        $event_html .= ")</i></h5></br>";
                        $event_html .= "<p style=\"color: #6D6D6D\">".$short_desc."</p>";

                        $image_url = "https://moisheritagelatinoamericain.ca/wp-content/uploads/2020/default/default_brochure.jpg";
                        if ($found_event['event_brochure'] != "") {
                            $image_url = $found_event['event_brochure'];
                        }
                        ?>
                        <div class='upcoming'>
                            <a href='https://moisheritagelatinoamericain.ca/en/home?p=<?php echo $post_id; ?>'>
                                <img class="event_image" src="<?php echo $image_url ?>" align="left"><?php echo $event_html; ?>
                            </a>
                        </div>

                        <?php
                        $i++;
                    }
                } else {
                    break;
                }
            }
        }
    } else {
        if ($current_locale == 'en_CA') {
            echo "There are no upcoming events.";
        } else if ($current_locale == 'es_ES') {
            echo "No hay eventos próximos.";
        } else if ($current_locale == 'fr_CA') {
            echo "Il n'y a aucun évènement à venir.";
        } else {
            echo "There are no upcoming events.";
        }
    }
?>
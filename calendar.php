<?php
/**
 *
 * The calendar script is used to display the calendar, and the number of the events for each day.
 * When the user clicks on the buttons for days with events, those events will appear below the calendar.
 *
 * @author Lin Yang, 2020 , yl2401293520@gmail.com
 * @author Liam Harbec, 2020, liam.harbec13@gmail.com
 *
 **/
?>

<style type="text/css">
    .navigation_button {
        border: 1px solid #3C4655;
        color: #6D6D6D;
        background-color: white;
        border-radius: 4px;
        box-shadow: 0 10px 12px 0 rgba(0,0,0,0.1), 0 13px 50px 0 rgba(0,0,0,0.05);
    }
    
    .navigation_button:hover {
        background-color: whitesmoke;
    }

    th,td {
        width: 100px;
        overflow: hidden;
        border: 1px solid #DDDDDD;
    }
    
    th {
        height: 5px;
    }
    
    td {
        padding: 0px;
        height: 100px;
    }
    
    strong {
        padding: 0px;
    }
    
    .events_message {
        border: #DDDDDD 1px solid;
        border-radius: 2px;
        color: red;
        box-shadow: 0 2px 3px 0 rgba(0,0,0,0.04), 0 2px 20px 0 rgba(0,0,0,0.02);
    }
    
    .events_message:hover {
        box-shadow: 0 2px 3px 0 rgba(0,0,0,0.04), 0 2px 20px 0 rgba(0,0,0,0.02);    }
    
    .event_category {
        border: solid 1px transparent;
        width: 100%;
    }
    
    .event_category:hover {
        border: solid 1px #BEBEBE;
        border-radius: 5px;
        background-color: whitesmoke;
        -webkit-box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
        -moz-box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
        box-shadow: 0px 3px 35px -9px rgba(0,0,0,0.45);
    }
    
    .event_description {
        display: inline-block;
        width: 78%;
        margin-top: 10px;
    }
    
    @media screen and (max-width: 1200px) {
        .event_description {
            width: 75%;
        }
    }
    

    @media screen and (max-width: 1100px) {
        .event_category {
            text-align: center;
        }
        
        .category_header {
            text-align: center;
        }
    }
	
@media screen and (max-device-width: 500px){
		table {
			table-layout:fixed;
			width:100%
		}

		.events_message:hover {
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.1), 0 5px 50px 0 rgba(0,0,0,0.05);
		}
		
		.events_message {
			border: #DDDDDD 1px solid;
			border-radius: 2px;
			color: red;
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.04), 0 5px 50px 0 rgba(0,0,0,0.02);
            text-align: 3px;
		}
	}	
	
@media screen and (max-device-width: 400px){
		table {
			table-layout:fixed;
			width:100%
		}

		.events_message:hover {
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.1), 0 5px 50px 0 rgba(0,0,0,0.05);
		}
		
		.events_message {
			border: #DDDDDD 1px solid;
			border-radius: 2px;
			color: red;
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.04), 0 5px 50px 0 rgba(0,0,0,0.02);
            text-align: 3px;
		}
	}
	@media screen and (max-device-width: 300px){
		table {
			table-layout:fixed;
			width:100%
		}

		.events_message:hover {
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.1), 0 5px 50px 0 rgba(0,0,0,0.05);
		}
		
		.events_message {
			border: #DDDDDD 1px solid;
			border-radius: 2px;
			color: red;
			box-shadow: 0 4px 5px 0 rgba(0,0,0,0.04), 0 5px 50px 0 rgba(0,0,0,0.02);
            text-align: 3px;
		}
	}
</style>
  
<?php
    //current date
    $dateComponents = getdate();
	
	//check does session already have month and year been saved
    if(isset($_SESSION['Calendar_month']) && isset($_SESSION['Calendar_year'])){
        $month = $_SESSION['Calendar_month'];
        $year = $_SESSION['Calendar_year'];
    }else{
        $month = $dateComponents['mon'];
        $year = $dateComponents['year'];
    }
	//check does events from session already loaded!
    if(isset($_SESSION['events'])){
        $events = $_SESSION['events'];
    }

    // Prints the calendar and events
    echo build_calendar($month, $year, $events);
    echo buildEventDiv();
	
	// This function will display the main part of calendar
	//@parameter: $month
	//@parameter: $year
	//@parameter: $events
    function build_calendar($month,$year,$events){
		//Getting the first dat of the month
        $firstDayOfMonth = mktime(0,0,0,$month,1,$year);
            
        //Getting the number of days this month contains
        $numberDays = date('t',$firstDayOfMonth);
            
        //Getting information about the first 
        $dateComponents = getdate($firstDayOfMonth);
            
        //Getting the name of month
        $monthName = $dateComponents['month'];
            
        //Getting the index value 0-6 of the first day of this month
        $dayOfWeek = $dateComponents['wday'];
        
		//Getting the current language the front page using		
        $current_locale = pll_current_language('locale');
		
		if ($current_locale == "en_CA") {
			//check does the website are open at moblie device
			if(isMobileDevice()){
				$daysOfWeek = array('Sun','Mon','Tue','Wed','Thu','Fri','Sat');
			}else{
				$daysOfWeek = array('Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday');
			}
			//Getting the current month by name such as June
			$monthName = $dateComponents['month'];
		} else if ($current_locale == "es_ES") {
			//check does the website are open at moblie device
			if(isMobileDevice()){
				$daysOfWeek = array('Dom','Lun','Mar','Mié','Jue','Vie','Sáb');
			}else{
				$daysOfWeek = array('Domingo','Lunes','Martes','Miércoles','Jueves','Viernes','Sábado');
			}
				if($month == 1){
					$monthName = 'Enero';
				}else if($month == 2){
					$monthName = 'Febrero';
				}else if($month == 3){
					$monthName = 'Marzo';
				}else if($month == 4){
					$monthName = 'Abril';
				}else if($month == 5){
					$monthName = 'Mayo';
				}else if($month == 6){
					$monthName = 'Junio';
				}else if($month == 7){
					$monthName = 'Julio';
				}else if($month == 8){
					$monthName = 'Agosto';
				}else if($month == 9){
					$monthName = 'Septiembre';
				}else if($month == 10){
					$monthName = 'Octubre';
				}else if($month == 11){
					$monthName = 'Noviembre';
				}else{
					$monthName = 'Diciembre';
				}
			
        } else {
			//check does the website are open at moblie device
			if(isMobileDevice()){
				$daysOfWeek = array('Dim','Lun','Mar','Mer','Jeu','Ven','Sam');
			}else{
				$daysOfWeek = array('Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi');
			}
			if($month == 1){
				$monthName = 'Janvier';
			}else if($month == 2){
				$monthName = 'Février';
			}else if($month == 3){
				$monthName = 'Mars';
			}else if($month == 4){
				$monthName = 'Avril';
			}else if($month == 5){
				$monthName = 'Mai';
			}else if($month == 6){
				$monthName = 'Juin';
			}else if($month == 7){
				$monthName = 'Juillet';
			}else if($month == 8){
				$monthName = 'Août ';
			}else if($month == 9){
				$monthName = 'Septembre';
			}else if($month == 10){
				$monthName = 'Octobre';
			}else if($month == 11){
				$monthName = 'Novembre'; 
			}else{
				$monthName = 'Décembre';
			}			
        } 

        //Getting the current date
        $datetoday = date('Y-m-d');
		
		//Seeting the month for last month
        $prve_month = date('m',mktime(0,0,0,$month-1,1,$year));
		
		//Seeting the year for last month
        $prve_year = date('Y',mktime(0,0,0,$month-1,1,$year));
		
		//Seeting the month for next month
        $next_month = date('m',mktime(0,0,0,$month+1,1,$year));
		
		//Seeting the year for next month
        $next_year = date('Y',mktime(0,0,0,$month+1,1,$year));
        
		
		// start coing the title for calendar
        $calendar = "<center><h2>$monthName $year</h2></center>";
        $calendar .= "<div style=\"width: 100%; display: flex; justify-content: center; align-items: center; flex-direction: column\">";
       
		//start coing the part holding the two button for last and next month
	    $calendar.= "<div>";
        $calendar.= "<form class='calendared' method='POST'>";
        $calendar.= "     <input onmouseover='buttonOnHover()' onmouseout='buttonOnUnhover' class='navigation_button' style='margin-right: 60px' type='submit' name='prev_Month' value='⇦'>";
                
        $calendar.= "     <input onmouseover='buttonOnHover()' onmouseout='buttonOnUnhover' class='navigation_button' type='submit' name='next_Month' value='⇨'>";
        
        //Javascript method to gaving the action for both button 
        $calendar.= "     <script type=\"text/javascript\">
                    function buttonOnHover() {
                        this.style.background = \"#E7E7E7\";
                        this.style.color = \"#3C4655\";
                    }
                    
                    function buttonOnUnhover() {
                        this.style.background = \"white\";
                        this.style.color = \"#6D6D6D\";
                    }
                </script>";
         $calendar.= "</form>";
        
		//POST method to set the session value if user press prev button 	
        if(isset($_POST['prev_Month'])){
            $_SESSION['Calendar_month'] = $prve_month;
            $_SESSION['Calendar_year'] = $prve_year;
            refreshPage();
        }
        //POST method to set the session value if user press next button 	
        if(isset($_POST['next_Month'])){
            $_SESSION['Calendar_month'] = $next_month;
            $_SESSION['Calendar_year'] = $next_year;
            refreshPage();
        }
			

        $calendar.= "</div><br>";
        $calendar.= "<table style=\"width: device-width \"><tr>";
            
        //Create the column headers
        foreach($daysOfWeek as $day){
            $calendar.= "<th class='header' style='background-color: red; color: white; width: 10px; text-align: center'>$day</th>";
        }
            
        $calendar.= "</tr><tr>";
            
        //initiating the day counter
        $currentDay = 1;
        if($dayOfWeek > 0){
            for($k = 0; $k<$dayOfWeek;$k++){
                $calendar.= "<td style='background-color: #DDDDDD'></td>";
            }
        }
		
       $month = str_pad($month,2,"0",STR_PAD_LEFT);
        while($currentDay <= $numberDays){
            if($dayOfWeek == 7){
                $dayOfWeek =0;
                $calendar.= "</tr><tr>";
            }
			
			//Getting the current day
            $currentDayRel = str_pad($currentDay,2,"0",STR_PAD_LEFT);
			
			//set up the date version 
            $date = "$year - $month - $currentDayRel";
			
			//Getting how many event does that day have.
            $eventCount = splitAndCompare($events,$year,$month,$currentDay);
            
			//Getting current language the front page are using
            $current_locale = pll_current_language('locale');
            $event_count_message;
            if ($current_locale == 'en_CA') {
				//setting the message for display in calendar,if event count more than 1 will add 's' in the end.
                if ($eventCount > 0) {
                    $event_count_message = $eventCount." event";
                    if ($eventCount > 1) {
                        $event_count_message .= "s";
                    }
                }
            } else if ($current_locale == 'es_ES') {
				//setting the message for display in calendar,if event count more than 1 will add 's' in the end.
                if ($eventCount > 0) {
                    $event_count_message = $eventCount." evento";
                    if ($eventCount > 1) {
                        $event_count_message .= "s";
                    }
                }
            } else if ($current_locale == 'fr_CA') {
				//setting the message for display in calendar,if event count more than 1 will add 's' in the end.
                if ($eventCount > 0) {
                    $event_count_message = $eventCount." événement";
                    if ($eventCount > 1) {
                        $event_count_message .= "s";
                    }
                }
            } else {
                if ($eventCount > 0) {
					//setting the message for display in calendar,if event count more than 1 will add 's' in the end.
                    $event_count_message = $eventCount." event";
                    if ($eventCount > 1) {
                        $event_count_message .= "s";
                    }
                }
            }
            
            if($eventCount >0 ){
				//Load the events information to the day.
                $eventsOnDay = eventOnDay($month,$year,$events,$currentDayRel);
				
				//Display the events into a new div 
                $divToAppend = loadEventDiv($month, $year, $eventsOnDay);
                
                $calendar.= "<td style='color: #3D4858; background-color: white'>";
				
				//Display the day into the calendar as number.
                $calendar.= "<strong>$currentDay</strong><br><br>";
				
				//If event number more than 1 and on moblie version, only show number of the events. 
				if(isMobileDevice()){
					$calendar .= "<button class=\"events_message\" onclick=\"toggleEvents('".$divToAppend."')\">➤ <i><br>$eventCount</i></button>";

				}else{
					//PC version display button for events in calendar.
					$calendar .= "<button class=\"events_message\" onclick=\"toggleEvents('".$divToAppend."')\">➤ <i>$event_count_message</i></button>";
				}
            }else{
                $calendar.= "<td style='color: #3D4858; background-color: white'><strong>$currentDay</strong></td>";
            }
            $currentDay++;
            $dayOfWeek++;
     }
        ?>
        
        <script type="text/javascript">
            function toggleEvents(oop) {
                $("#eventDiv").empty();
                $("#eventDiv").append(oop);
            }
        </script>

        <?php
            if($dayOfWeek !=7){
                $remainingDays = 7-$dayOfWeek;
                for($i=0;$i<$remainingDays;$i++){
                    $calendar.= "<td style='background-color: #DDDDDD'></td>";
                }
            }
            $calendar.= "</tr></table></div>";
			return $calendar;
        }

/**
 * This function will split the event date and compare with date given. return the number of event for that day. 
 **/
    function splitAndCompare($events,$year,$month,$day){
        $count = 0;
        foreach($events as $key => $event){
			if($event['approved'] == 1){
				$newDateArray =explode('-',$event['start_date']);
				if($year == $newDateArray[0] && $month ==$newDateArray[1] && $day == $newDateArray[2]) {
					$count ++;
				}
			}
		}
        return $count;
    }

	/**
	 * Load the current device and return boolean is it a moblie device
	 * return false if it's not moblie
	 * return true if it is moblie
	 **/
	function isMobileDevice() {
		return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
	}
	
    /**
    * Creates the div that will contain the events of a given day
    */
    function buildEventDiv() {
        echo "<div style=\"margin: 0 5%\" id=\"eventDiv\">";
        echo "</div>";
    }

    /**
    * Returns an array of all the events on a given day
    */
    function eventOnDay ($month,$year,$events,$day){
        foreach ($events as $key => $event){
			if ($event['approved'] == 1){
				$newDateArray = explode('-',$event['start_date']);
				if ($year == $newDateArray[0] && $month == $newDateArray[1] && $day == $newDateArray[2]) {
					$newEventArray[$key] = $event;
                }
            }
		}
        return $newEventArray;   
    }

    /**
    * Loads the events of a given day into the eventsDiv
    */
    function loadEventDiv($month, $year, $eventsOnDay) {
        $current_locale = pll_current_language('locale');
        $event_html;
        
        foreach ($eventsOnDay as $event) {
            
            $retrieved_start_date = $event['start_date'];
            $start_date_format = DateTime::createFromFormat('Y-m-d', $retrieved_start_date);
            
            $desc;
            $short_desc;
            $desc;
            $ft;
            $at;
            $post_id;
            if ($current_locale == 'en_CA') {
                $desc = nl2br($event['event_description_english']);
                setlocale(LC_TIME, "en_CA");
                $format = "%B %e, %Y";
                $at = " at ";
                $post_id = $event['post_id_en'];
            } else if ($current_locale == 'es_ES') {
                $desc = nl2br($event['event_description_spanish']);
                setlocale(LC_TIME, "es_ES");
                $format = "%e de %B, %Y";
                $at = " a ";
                $post_id = $event['post_id_es'];
            } else if ($current_locale == 'fr_CA') {
                $desc = nl2br($event['event_description_french']);
                setlocale(LC_TIME, "fr_CA");
                $format = "%e %B, %Y";
                $at = " à ";
                $post_id = $event['post_id_fr'];
            } else {
                $desc = nl2br($event['event_description_english']);
                setlocale(LC_TIME, "en");
                $format = "%e %B, %Y";
                $at = " at ";
                $post_id = $event['post_id_en'];
            }
            
            $short_desc = $desc;
            if (strlen($desc) > 130) {
                $short_desc = substr($desc, 0, 130);
                if (rtrim($short_desc) != $short_desc) {
                    $short_desc = rtrim($short_desc)."...";
                } else {
                    $short_desc .= "...";
                }
            }
            $timestamp = strtotime($start_date_format->format("m/d/Y"));
            $start_date = strftime($format, $timestamp);
            $event_desc_html = "<h5 style=\"margin-bottom: -25px\">";
            $event_desc_html .= $event['event_name'];
            $event_desc_html .= "<i style=\"font-size: 12px; color: #AAAAAA; margin-top: -10px\"> (";
            $event_desc_html .= $start_date;
            $event_desc_html .= $at;
            $event_desc_html .= $event['start_time'];
            if (isset($event['end_time'])) {
                $event_desc_html .= "-".$event['end_time'];
            }
            $event_desc_html .= ")</i></h5></br>";
            $event_desc_html .= "<p style=\"color: #6D6D6D\">".$short_desc."</p>";
            $image_url = "https://moisheritagelatinoamericain.ca/wp-content/uploads/2020/default/default_brochure.jpg";
            if ($event['event_brochure'] != "") {
                $image_url = nl2br($event['event_brochure']);
            }
            
            $event_html .= '<div class="event_category">';
            $event_html .= '<a href="https://moisheritagelatinoamericain.ca/en/home?p='.$post_id.'">';
            $event_html .= '<div style="padding: 10px">';
            $event_html .= '<img style="height: 100px; width: 100px; display: inline-block; vertical-align: top; margin-right: 10px; border-radius: 3px 25px 3px" src="'.$image_url.'">';
            $event_html .= '<div class="event_description">'.fixSpacing($event_desc_html).'</div>';
            $event_html .= '</div>';
            $event_html .= '</a>';
            $event_html .= '</div>';
        }
        return htmlspecialchars($event_html);
    }

    /**
    * Replaces all strange newline characters, as well as non-breaking spaces with
    * regular <br> and space characters
    */
    function fixSpacing($description) {
        $rn = str_replace("\r\n", "<br>", $description);
        $nr = str_replace("\n\r", "<br>", $rn);
        $r = str_replace("\r", "<br>", $nr);
        $n = str_replace("\n", "<br>", $r);
        $space = str_replace(" ", " ", $n);
        $nbsp = str_replace("&nbsp;", " ", $space);
        return $nbsp;
    }

    // JavaScript function to refresh the page and remove an event that has been approved/deleted from the displayed list of "new events"
    function refreshPage() {
        echo "<script type\"text/javascript\">
            window.onload = function() {
                if(!window.location.hash) {
                    window.location = window.location + '#calendar';
                    window.location.reload();
                }
            }
            removeHash();
            
            function removeHash () { 
                var scrollV, scrollH, loc = window.location;
                if (\"pushState\" in history)
                    history.pushState(\"\", document.title, loc.pathname + loc.search);
                else {
                    // Prevent scrolling by storing the page's current scroll offset
                    scrollV = document.body.scrollTop;
                    scrollH = document.body.scrollLeft;

                    loc.hash = \"\";

                    // Restore the scroll offset, should be flicker free
                    document.body.scrollTop = scrollV;
                    document.body.scrollLeft = scrollH;
                }
            }
            </script>";
    }
?>